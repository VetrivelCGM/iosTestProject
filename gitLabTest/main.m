//
//  main.m
//  gitLabTest
//
//  Created by SSL on 11/20/15.
//  Copyright © 2015 CGM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
