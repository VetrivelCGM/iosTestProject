//
//  AppDelegate.h
//  gitLabTest
//
//  Created by SSL on 11/20/15.
//  Copyright © 2015 CGM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

